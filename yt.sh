#!/bin/bash
while true; do
echo -n '>'; read option
if [ "$option" == "help" ]; then
echo 'list sub - list video from subscribed'
echo 'list channels - list subscribed channels'
echo 'watch sub - watch video from list sub id'
echo 'listen sub - listen instead of watch'
echo 'download sub - download instead of watch'
echo 'download sub audio - download audio'
echo 'add sub - subscribe to a channel'
echo 'remove sub - unsubscribe from a channel'
echo 'search - search for video'
echo 'search page - change page of searches'
echo 'watch search - watch video from search'
echo 'listen search - listen to audio from search'
echo 'download search - download video from search'
echo 'download search audio - download audio from search'
echo 'search interactive - open youtube-viewer'
echo 'help - display this help'
echo 'search comments - display comments of video in search'
echo 'search comments newest - display newest comments of video in search'
fi
if [ "$option" == "list sub" ]; then
ytcc --cleanup
ytcc -u
ytcc -ln --no-header | sort -rn | less -RS
fi
if [ "$option" == "list channels" ]; then
ytcc -c | less -RS
fi
if [ "$option" == "watch sub" ]; then
echo -n '=>'; read subid
ytcc -w $subid -y
unset subid
fi
if [ "$option" == "download sub" ]; then
echo -n '=>'; read subid
ytcc -d $subid -y
unset subid
fi
if [ "$option" == "add sub" ]; then
echo -n 'Name=>'; read name
echo -n 'URL=> '; read url
ytcc -a "$name" "$url"
unset name url
fi
if [ "$option" == "remove sub" ]; then
echo -n 'Name=>'; read name
ytcc -r "$name"
unset name
fi
if [ "$option" == "listen sub" ]; then
echo -n '=>'; read subid
ytcc -w $subid -x -y
unset subid
fi
if [ "$option" == "download sub audio" ]; then
echo -n '=>'; read subid
ytcc -d $subid -x -y
unset subid
fi
if [ "$option" == "search" ]; then
echo -n '=>'; read search
youtube-viewer --no-I --results=50 "$search" | less -RS
page=1
fi
if [ "$option" == "search interactive" ]; then
youtube-viewer --results=50
fi
if [ "$option" == "search page" ]; then
if [ -z "$search" ]; then echo "Nothing has been searched."; else
echo -n '=>'; read page
youtube-viewer --no-I --results=50 --page="$page" "$search" | less -RS
fi
fi
if [ "$option" == "watch search" ]; then
if [ -z "$search" ]; then echo "Nothing has been searched."; else
echo -n '=>'; read searchid
youtube-viewer --no-I --results=50 --page="$page" --std-input="$searchid" "$search"
unset searchid
fi
fi
if [ "$option" == "listen search" ]; then
if [ -z "$search" ]; then echo "Nothing has been searched."; else
echo -n '=>'; read searchid
youtube-viewer --no-I -n --results=50 --page="$page" --std-input="$searchid" "$search"
unset searchid
fi
fi
if [ "$option" == "download search" ]; then
if [ -z "$search" ]; then echo "Nothing has been searched."; else
echo -n '=>'; read searchid
youtube-viewer --no-I -d --results=50 --page="$page" --std-input="$searchid" "$search"
unset searchid
fi
fi
if [ "$option" == "download search audio" ]; then
if [ -z "$search" ]; then echo "Nothing has been searched."; else
echo -n '=>'; read searchid
youtube-viewer --no-I -d -n --results=50 --page="$page" --std-input="$searchid" "$search"
unset searchid
fi
fi
if [ "$option" == "search comments" ]; then
if [ -z "$search" ]; then echo "Nothing has been searched."; else
echo -n '=>'; read searchid
URL=$(youtube-viewer --no-I -e=*URL* --page="$page" --std-input="$searchid" "$search" | grep "^https")
youtube-viewer --no-I --comments="$URL" --comments-order=relevance | less -RS
unset searchid URL
fi
fi
if [ "$option" == "search comments newest" ]; then
if [ -z "$search" ]; then echo "Nothing has been searched."; else
echo -n '=>'; read searchid
URL=$(youtube-viewer --no-I -e=*URL* --page="$page" --std-input="$searchid" "$search" | grep "^https")
youtube-viewer --no-I --comments="$URL" --comments-order=time | less -RS
unset searchid URL
fi
fi
done
